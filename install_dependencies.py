#!/usr/bin/python

import os
import sys
import urllib
import zipfile
import shutil
import subprocess

CMAKE_URL = 'http://www.cocos2d-x.org/filedown/cocos2d-x-3.8.1.zip'
COCOS_ROOT = os.path.abspath(os.path.join(__file__, '..', 'GameSparksSample', 'cocos2d'))
cocos_package = os.path.abspath(os.path.join(__file__, '..', CMAKE_URL.split('/')[-1]))


def download(url, destination):

	last_percentage = [-1]
	def reporthook(count, block_size, total_size):
		if sys.stdout.isatty():
			percentage = int(1000.0 * count * block_size / total_size)/10.0
		else: # if output is redirected (jenkins) don't print so often
			percentage = int(10.0 * count * block_size / total_size)*10.0

		if percentage != last_percentage[0]:
			sys.stdout.write('\rdownloaded {0} bytes of {1} bytes ({2:.1f}%)               '.format(
				count*block_size,
				total_size,
				percentage
			))
		last_percentage[0] = percentage

	urllib.urlretrieve(url, destination, reporthook)
	print 'download done'


def unzip(package, dst):
	print('unzipping "%s"' % package)
	with zipfile.ZipFile(package, 'r') as zf:
		root_in_zip = zf.namelist()[0].split('/')[0]
		zf.extractall()
		shutil.move( root_in_zip, dst)
	print('[done]')

def install_cocos():
	if not os.path.exists(os.path.join(COCOS_ROOT, 'AUTHORS')):
		if not os.path.exists(cocos_package):
			download(CMAKE_URL, cocos_package)

		unzip(cocos_package, COCOS_ROOT )
		os.unlink(cocos_package)

def download_cocos_dependencies():
	if os.path.exists(os.path.join(COCOS_ROOT, 'external', 'png')):
		print('skipping installing dependencies')
		return

	cwd = os.getcwd()

	os.chdir( COCOS_ROOT )

	subprocess.check_call([
		'python',
		'./download-deps.py',
		'--remove-download=yes',
	])

	os.chdir(cwd)

if __name__ == '__main__':
	install_cocos()
	download_cocos_dependencies()
